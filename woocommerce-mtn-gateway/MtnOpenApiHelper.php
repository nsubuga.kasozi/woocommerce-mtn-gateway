<?php
function GenerateUUID()
{
    return sprintf(
        '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),

        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,

        // 48 bits for "node"
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff)
    
    );
}

function GenerateToken($uuid, $apikey, $subscription_key)
{
    // This sample uses the Apache HTTP client from HTTP Components (http://hc.apache.org/httpcomponents-client-ga/)
    //require_once 'HTTP/Request2.php';

    $plain = "$uuid:$apikey";
    WriteToLog("plain: $plain");

    $base64 =base64_encode($plain);
    WriteToLog("base64: $base64");

    $authorization_header = "Basic $base64";
    WriteToLog("AuthHeader: $authorization_header");

    $headers = array(
                        // Request headers
                        'Authorization' => $authorization_header,
                        'Ocp-Apim-Subscription-Key' => "$subscription_key",
                    );

    $post_request = array(
        'method' => 'POST',
        'headers' => $headers,
        'timeout' => 60,
        'body' => "",
    );

    WriteToLog($post_request);
    $response = wp_remote_post('https://ericssonbasicapi2.azure-api.net/collection/token/', $post_request);

    WriteToLog($response);
    $tokenstring = json_decode($response['body'], true);

    return $tokenstring['access_token'];
}

function SendRequestToPay($uuid, $token, $subscription_key, $amount, $phone)
{
    //require_once 'HTTP/Request2.php';

    $url="https://ericssonbasicapi2.azure-api.net/collection/v1_0/requesttopay";

    $headers = array(
                        // Request headers
                        'Authorization' => "Bearer $token",
                        'X-Callback-Url' => "http://localhost/",
                        'X-Reference-Id' => "$uuid",
                        'X-Target-Environment' => "sandbox",
                        'Content-Type' => 'application/json',
                        'Ocp-Apim-Subscription-Key' => "$subscription_key",
                    );


    $body = '{
                "amount": "'.$amount.'",
                "currency": "EUR",
                "externalId": "12567788994565667g5659678",
                "payer": {
                "partyIdType": "MSISDN",
                "partyId": "'.$phone.'"
                },
                "payerMessage": "Test Payment 5",
                "payeeNote": "Test Payment 6"
            }';


    $post_request = array(
                'method' => 'POST',
                'headers' => $headers,
                'timeout' => 60,
                'body' => $body,
            );

    WriteToLog($post_request);

    $response = wp_remote_post($url, $post_request);
    WriteToLog($response);

    $tokenstring = json_decode($response['body'], true);
    WriteToLog("TokenString: $tokenstring");

    return $tokenstring;
}

function GetTransactionStatus($uuid, $token, $subscription_key)
{
    $url="https://ericssonbasicapi2.azure-api.net/collection/v1_0/requesttopay/$uuid";

    $headers = array(
                        // Request headers
                        'Authorization' => "Bearer $token",
                        'X-Target-Environment' => 'sandbox',
                        'Ocp-Apim-Subscription-Key' => "$subscription_key",
                    );


    $body = '';
    
    $post_request = array(
                'method' => 'GET',
                'headers' => $headers,
                'timeout' => 60,
                'body' => $body,
            );

    WriteToLog($post_request);

    $response = wp_remote_get($url, $post_request);
    WriteToLog($response);

    $tokenstring = json_decode($response['body'], true);
    WriteToLog("Transaction Status: $tokenstring");

    $Status = $tokenstring['status'];
    WriteToLog("Status: $Status");
    
    return $Status;
}
