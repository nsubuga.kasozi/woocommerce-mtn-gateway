<?php
class WC_MTN_Gateway extends WC_Payment_Gateway
{
    //constants
    public $SubscriptionKey = "c091e5095e484fdea549fa23f40b11ea";
    public $ApiKey = "0f194c7ed6ce4b59928abb9e72abeb1e";
    public $ApiUser = "4e1ec7a2-9339-438b-855f-55472183e3ef";
    public $title = "MTN Mobile Money";
    public $description = "Pay with Mtn Mobile Money";
    public $plugin_ID= "'WC-Mtn'";

    public function __construct()
    {
        $this->id = $this->plugin_ID; // payment gateway plugin ID
        $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
        $this->has_fields = true; // in case you need a custom credit card form
        $this->method_title = $this->title;
        $this->method_description = $this->description; // will be displayed on the options page

        // gateways can support subscriptions, refunds, saved payment methods,
        // but in this tutorial we begin with simple payments
        $this->supports = array(
                                'products'
                            );

        // Method with all the options fields
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();
        $this->title = $this->title;
        $this->description = $this->description;
        $this->enabled =  $this->get_option('enabled');
        $this->testmode = 'no'; //'yes' === $this->get_option('testmode');
        $this->private_key = $this->testmode ? $this->get_option('test_private_key') : $this->get_option('private_key');
        $this->publishable_key = $this->testmode ? $this->get_option('test_publishable_key') : $this->get_option('publishable_key');

        // This action hook saves the settings
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ));

        // We need custom JavaScript to obtain a token
        add_action('wp_enqueue_scripts', array( $this, 'payment_scripts' ));

        // You can also register a webhook here
        // add_action( 'woocommerce_api_{webhook name}', array( $this, 'webhook' ) );
    }
    

    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title'       => 'Enable/Disable',
                'label'       => 'Enable MTN Payment Gateway',
                'type'        => 'checkbox',
                'description' => '',
                'default'     => 'no'
            ),
            'title' => array(
                'title'       => 'Title',
                'type'        => 'text',
                'description' => 'This controls the title which the user sees during checkout.',
                'default'     => 'MTN Mobile Money',
                'desc_tip'    => true,
            ),
            'description' => array(
                'title'       => 'Description',
                'type'        => 'textarea',
                'description' => 'This controls the description which the user sees during checkout.',
                'default'     => 'Pay with MTN Mobile Money',
            ),
            'testmode' => array(
                'title'       => 'Test mode',
                'label'       => 'Enable Test Mode',
                'type'        => 'checkbox',
                'description' => 'Place the payment gateway in test mode using test API keys.',
                'default'     => 'yes',
                'desc_tip'    => true,
            ),
            'test_publishable_key' => array(
                'title'       => 'Test Publishable Key',
                'type'        => 'text'
            ),
            'test_private_key' => array(
                'title'       => 'Test Private Key',
                'type'        => 'password',
            ),
            'publishable_key' => array(
                'title'       => 'Live Publishable Key',
                'type'        => 'text'
            ),
            'private_key' => array(
                'title'       => 'Live Private Key',
                'type'        => 'password'
            )
        );
    }

    
    public function payment_fields()
    {
        // ok, let's display some description before the payment form
        if ($this->description) {
            // you can instructions for test mode, I mean test card numbers etc.
            if ($this->testmode) {
                $this->description .= ' TEST MODE ENABLED.';
                $this->description  = trim($this->description);
            }
            // display the description with <p> tags etc.
            echo wpautop(wp_kses_post($this->description));
        }

        // I will echo() the form, but you can close PHP tags and print it directly in HTML
        echo '<fieldset id="wc-' . esc_attr($this->id) . '-cc-form" class="wc-credit-card-form wc-payment-form" style="background:transparent;">';

        // Add this action hook if you want your custom payment gateway to support it
        do_action('woocommerce_credit_card_form_start', $this->id);

        // I recommend to use inique IDs, because other gateways could already use #ccNo, #expdate, #cvc
        echo '<div class="form-row form-row-wide"><label>Phone Number <span class="required">*</span></label>
                <input id="mtn_phone_number" name="mtn_phone_number" type="text" autocomplete="off">
                </div>';

        do_action('woocommerce_credit_card_form_end', $this->id);

        echo '<div class="clear"></div></fieldset>';
    }

    
    public function payment_scripts()
    {
        // we need JavaScript to process a token only on cart/checkout pages, right?
        if (! is_cart() && ! is_checkout() && ! isset($_GET['pay_for_order'])) {
            return;
        }

        // if our payment gateway is disabled, we do not have to enqueue JS too
        if ('no' === $this->enabled) {
            return;
        }

        // no reason to enqueue JavaScript if API keys are not set
        if (empty($this->private_key) || empty($this->publishable_key)) {
            return;
        }

        // do not work with card detailes without SSL unless your website is in a test mode
        if (! $this->testmode && ! is_ssl()) {
            return;
        }

    }

    
    public function validate_fields()
    {
        //get phone number supplied
        $phone = $_POST[ 'mtn_phone_number' ];

        //oops..phone number is empty
        if (empty($phone)) {
            wc_add_notice('Please Supply an MTN Phone Number', 'error');
            return false;
        }

        //phone number is too short
        if (strlen($phone)!=12) {
            wc_add_notice('Please enter the number begining with 256. e.g 256785975800', 'error');
            return false;
        }

        return true;
    }

    
    public function process_payment($order_id)
    {
        try {
            global $woocommerce;

            // we need it to get any order detailes
            $order = wc_get_order($order_id);
        
            WriteToLog("order details: $order");

            include_once('MtnOpenApiHelper.php');

            $amount = wc_format_decimal($order->get_total(), 2);
            WriteToLog("Amount: $amount");

            $phone = $_POST['mtn_phone_number'];
            WriteToLog("Phone: $phone");

            //generate a transaction id for this request
            $uuid = GenerateUUID();
            WriteToLog("UUID: $uuid");

            //generate a token for this session
            $token = GenerateToken($this->ApiUser, $this->ApiKey, $this->SubscriptionKey);
            WriteToLog("Token: $token");

            //send a request to pay to the phone number supplied
            $payResposne = SendRequestToPay($uuid, $token, $this->SubscriptionKey, $amount, $phone);
            WriteToLog("payResponse: $payResposne");

            //get the status of a previously supplied transaction
            $tranStatus = GetTransactionStatus($uuid, $token, $this->SubscriptionKey);
            WriteToLog("tranStatus: $tranStatus");

            
            // it failed at mtn
            if ($tranStatus == 'FAILED') {
                WriteToLog("body Status is Failed");
                wc_add_notice('Please try again.', $body['reason']);
                return;
            }

            //success at MTN
            WriteToLog("All is good");

            // we received the payment
            $order->payment_complete();
            $order->reduce_order_stock();

            // some notes to customer (replace true with false to make it private)
            $order->add_order_note('Hey, your order is paid! Thank you!', true);

            // Empty cart
            $woocommerce->cart->empty_cart();

            // Redirect to the thank you page
            return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                );
                
        } catch (Exception $ex) {
            WriteToLog(ex);
            wc_add_notice('Please try again.', 'error');
            return;
        }
    }

    
    public function webhook()
    {
    }
}
