<?php
/**
 * woocommerce-mtn-gateway Plugin is the simplest WordPress plugin for beginner.
 * Take this as a base plugin and modify as per your need.
 *
 * @package woocommerce-mtn-gateway Plugin
 * @author woocommerce-mtn-gateway
 * @license GPL-2.0+
 * @link https://woocommerce-mtn-gateway.com/tag/wordpress-beginner/
 * @copyright 2017 Kasozi, LLC. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: woocommerce-mtn-gateway Plugin
 *            Plugin URI: https://woocommerce-mtn-gateway.com/tag/wordpress-beginner/
 *            Description: woocommerce-mtn-gateway Plugin
 *            Version: 3.0
 *            Author: Kasozi
 *            Author URI: https://woocommerce-mtn-gateway.com/
 *            Text Domain: woocommerce-mtn-gateway
 *            Contributors: woocommerce-mtn-gateway
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */
 
 if (!defined('ABSPATH')) {
     die;
 }

 
add_action('plugins_loaded', 'mtn_init_gateway_class');

function mtn_init_gateway_class()
{
    //if condition use to do nothin while WooCommerce is not installed
    if (!class_exists('WC_Payment_Gateway')) {
        return;
    }

    include_once( 'WC_MTN_Gateway.php' );

    // class add it too WooCommerce
    add_filter('woocommerce_payment_gateways', 'add_mtn_payment_gateway');
    
    function add_mtn_payment_gateway($methods)
    {
        $methods[] = 'WC_MTN_Gateway';
        return $methods;
    }
}

//writes stuff out to debug.log
if (!function_exists('WriteToLog')) {
    function WriteToLog($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}
